var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    minifycss = require('gulp-minify-css');

var config = {
    'jsPath': 'public/javascripts/',
    'cssPath': 'public/stylesheets/'
};

/**
 * Minify js file
 */
gulp.task('prepare-js', function() {
    return gulp.src(config.jsPath + '/app.js')
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify({mangle: false}))
        .pipe(gulp.dest(config.jsPath));
});

/**
 * Minify css file
 */
gulp.task('prepare-css', function() {
    return gulp.src(config.cssPath + '/style.css')
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest(config.cssPath ));
});

gulp.task('default', function() {
    gulp.start(
        'prepare-js',
        'prepare-css'
    );
});