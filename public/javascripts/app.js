angular.module('api', [])
    .controller('mainController', function ($scope, $http) {
        $scope.formData = {};
        $scope.usersData = {};
        $scope.user = {};

        // Get all users
        $http.get('/api/users')
            .success(function (data) {
                $scope.usersData = data;
            })
            .error(function (error) {
                console.log('Error: ' + error);
            });

        // Delete a user
        $scope.deleteUser = function (userId) {
            $http.delete('/api/users/' + userId)
                .success(function (data) {
                    $scope.usersData = data;
                })
                .error(function (data) {
                    console.log('Error: ' + data);
                });
        };

        $scope.clickUpdate = function (data) {
            $scope.user = data;
            $scope.formData = data;
        };

        $scope.clickSave = function () {
            if (!$.isEmptyObject($scope.user)) {
                // Update user
                $http.put('/api/users/' + $scope.user.id, $scope.formData)
                    .success(function (data) {
                        $scope.formData = {};
                        $scope.user = {};
                        $scope.usersData = data;
                    })
                    .error(function (error) {
                        console.log('Error: ' + error);
                    });
            } else {
                // Create a new user
                console.log('save')
                $http.post('/api/users', $scope.formData)
                    .success(function (data) {
                        $scope.formData = {};
                        $scope.usersData = data;
                    })
                    .error(function (error) {
                        console.log('Error: ' + error);
                    });
            }
            $('#modal').modal('hide')
        }
    });
