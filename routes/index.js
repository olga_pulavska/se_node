var express = require('express'),
    mysql = require('mysql'),
    path = require('path'),
    router = express.Router();

var pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'api'
});

/* GET home page. */
router.get('/', function (req, res, next) {
    res.sendFile(path.join(__dirname, '../views', 'index.html'));
});

/*Create user*/
router.post('/api/users', function (req, res) {
    // Grab data from http request
    var data = {name: req.body.name, age: req.body.age};
    pool.getConnection(function (err, connection) {
        console.log(err);
        // Use the connection
        connection.query('INSERT INTO `users`(`name`,`age`) VALUES (?, ?)', [data.name, data.age], function (err) {
            console.log(err);
            connection.query('SELECT * FROM users ORDER BY name ASC', function (err, rows) {
                console.log(err);
                connection.release();
                return res.json(rows);
            });
        });
    });
});
/*Get Users*/
router.get('/api/users', function (req, res) {
    pool.getConnection(function (err, connection) {
        console.log(err);
        connection.query('SELECT * FROM users ORDER BY name ASC', function (err, rows) {
            console.log(err);
            connection.release();
            return res.json(rows);
        });
    });
});
/*Update users*/
router.put('/api/users/:id', function(req, res) {
    // Grab data from the URL parameters
    var id = req.params.id;
    // Grab data from http request
    var data = {name: req.body.name, age: req.body.age};

    pool.getConnection(function (err, connection) {
        console.log(err);
        // Use the connection
        connection.query('UPDATE `users` SET `name`= ?,`age`= ?  WHERE `id`= ?', [data.name, data.age, id], function (err) {
            console.log(err);
            connection.query('SELECT * FROM users ORDER BY name ASC', function (err, rows) {
                console.log(err);
                connection.release();
                return res.json(rows);
            });
        });
    });
});
/*Delete user*/
router.delete('/api/users/:id', function(req, res) {
    var id = req.params.id;

    pool.getConnection(function (err, connection) {
        console.log(err);
        // Use the connection
        connection.query('DELETE FROM `users` WHERE `id`= ?', id, function (err) {
            console.log(err);
            connection.query('SELECT * FROM users ORDER BY name ASC', function (err, rows) {
                console.log(err);
                connection.release();
                return res.json(rows);
            });
        });
    });
});


module.exports = router;
